public class quadratic {
    public static void main(String[] args) {
    	double a = Double.parseDouble(args[0]);
    	double b = Double.parseDouble(args[1]);
    	double c = Double.parseDouble(args[2]);
    	double diskriminant = (b*b - 4*a*c);
    	if (diskriminant < 0) {
            System.out.println("diskriminant je mensi nez nula");
    	} else {
	        double x1 = (b*(-1) + Math.sqrt(diskriminant))/(2*a);
	        double x2 = (b*(-1) - Math.sqrt(diskriminant))/(2*a);
            if (x1 == x2) {
            	System.out.printf("oba koreny jsou %20.16f\n", x1);
            } else {
            	System.out.printf("x1 = %20.16f x2 = %20.16f\n", x1, x2);
            }
	    }
    }
}