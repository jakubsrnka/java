public class thumbnail {
    public static void main(String[] args) {
    	awh.Image img = awh.Image.loadFromFile(args[0]);
    	double ratio = 1.5;
    	double height = (double) img.getWidth();
    	double width =  (double) img.getHeight();
    	double imgRatio = height/width;

            	System.out.printf("%20.16f\n", imgRatio);

    	if (ratio == imgRatio) {
    		img.rescale(150, 100);
    	} else {
    		awh.Color gray = awh.Color.fromRgb((int) (128), (int) 128, (int)128);
    		awh.Image thumbnail = awh.Image.createEmpty​(150, 100, gray);
    		if (imgRatio < ratio) {
    			double newWidth = 150*(imgRatio/ratio);
    			img.rescale((int) Math.round(newWidth), 100);
    			double indent = (150 - img.getWidth())/2;
    			thumbnail.pasteFrom(img, (int) Math.round(indent), 0);
    		} else if (imgRatio > ratio) {
    			double newHeight = 100*(ratio/imgRatio);
    			img.rescale(150, (int) Math.round(newHeight));
    			double indent = (100 - img.getHeight())/2;
    			thumbnail.pasteFrom(img, 0, (int) Math.round(indent));
    		}
    		img = thumbnail;
    	}


    	img.saveToFile(args[1]);


    }
}