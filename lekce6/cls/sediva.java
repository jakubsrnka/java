public class sediva {
    public static void main(String[] args) {
    	awh.Image img = awh.Image.loadFromFile(args[0]);
    	int height = img.getHeight();
    	int width = img.getWidth();

    	for (int i = 0; i < width; i++) {
    		for (int j = 0; j < height; j++) {
    			awh.Color color = img.getPixel(i, j);
    			int grey = (int) (.299*color.getRed() + .587*color.getGreen() + .114*color.getBlue());
    			awh.Color newColor = awh.Color.fromRgb(grey, grey, grey);
    			img.setPixel(i, j, newColor);
    		}
    		
    	}

    	img.saveToFile(args[0]);
    }
}