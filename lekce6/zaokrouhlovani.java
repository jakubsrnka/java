public class zaokrouhlovani {
    public static void main(String[] args) {
        /*
         * Změňte následující na 2, 4 nebo 5 a pozorujte rozdíly.
         * A hlavně: JE NEZAPOMEŇTE!


         * při použítí Math.round() se bude cislo rovnat ocekavam
         */
        int kroku = 5;
        double zdibec = 1. / kroku;
        double cislo = 0.;
        for (int i = 0; i < 25 * kroku; i++) {
            if ((i % kroku) == 0) {
                double ocekavam = i / kroku;
                if (Math.round(cislo) == ocekavam) { // zde je přidáno Math.round()
                    System.out.printf("==> %.0f je ok\n", cislo);
                } else {
                    System.out.printf("%20.16f != %20.16f\n", ocekavam, cislo);
                }
            }
            cislo += zdibec;
        }
    }
}