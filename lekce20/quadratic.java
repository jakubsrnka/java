import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import cz.alisma.alej.prog.QuadraticEquationGenerator;
import cz.alisma.alej.prog.QuadraticEquationPrinter;

public class quadratic {
	/*private static class HtmlPrinter implements QuadraticEquationPrinter {
		private void WriteHtml(String html) throws IOException {
			try {
				FileWriter writer = new FileWriter("quadratic.html", true);
				writer.write(html);
			} catch (IOException e) {
				System.out.println("you got an error m8");
			}
		}
		public void printHeader(int count) {
			WriteHtml("<!DOCTYPE html> <html> <head> <title>" + count + " random quadratic equation</title> <style</head> <body>");
		}
		public void printEquation(int a, int b, int c, int x1, int x2) {
			WriteHtml("<p>" + a + x1 + "<sup>2</sup> + " + b + x2 + " + " + c + "</p>");
		}
		public void printFooter() {
			WriteHtml("</body> </html>");
		}
	}*///how???? throws IOException even tho it has thy & catch in WriteHtml
	private static class ScreenPrinter implements QuadraticEquationPrinter {
		public void printHeader(int count) {}
		public void printEquation(int a, int b, int c, int x1, int x2) {
			System.out.println(a + "(" + x1 + ")^2 + " + b + "(" + x2 + ") + " + c);
		}
		public void printFooter() {}
	}
	public static void main(String[] args) {
        // QuadraticEquationPrinter printer = new HtmlPrinter();
         QuadraticEquationPrinter printer = new ScreenPrinter();
 
        QuadraticEquationGenerator.generate(10, printer);
	}
}