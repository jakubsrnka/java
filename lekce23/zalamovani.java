import java.util.Scanner;

public class zalamovani {
    private static final int WIDTH = 25;
  
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
         
        int width = -1;
        while (sc.hasNextLine()) {
        	String line = sc.nextLine();
        	if (line.isEmpty()) {
        		System.out.println("\n");
        		width = -1;
        	}
             
            Scanner words = new Scanner(line);         
             
            while (words.hasNext()) {
                String word = words.next();

                if (width + word.length() + 1 > WIDTH) {
                    System.out.println();
                    width = -1;
                }

                if (width > 0) {
                    System.out.print(" ");
                }

                System.out.printf("%s", word);
                width += 1 + word.length();
            }
        }
         
        if (width > 0) {
            System.out.println();
        }
    }
}