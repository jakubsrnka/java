import java.io.FileReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;


public class preklad {
	public static void main(String[] args) throws IOException {
		if (args.length == 2) {
			try {
				FileReader dictionaryFile = new FileReader(args[0]);
				FileReader toTranslateFile = new FileReader(args[1]);

				java.util.Scanner dictionaryScanner = new java.util.Scanner(dictionaryFile);
				Map<String, String> dictionary = new HashMap<String, String>();

				while (dictionaryScanner.hasNextLine()) {
					String nextWord = dictionaryScanner.nextLine();
					String[] translations = nextWord.split(" ");
					dictionary.put(translations[0], translations[1]);
				}

				java.util.Scanner toTranslateScanner = new java.util.Scanner(toTranslateFile);
				while (toTranslateScanner.hasNextLine()) {
					String nextLine = toTranslateScanner.nextLine();
					String[] words = nextLine.split(" ");
					for (int i = 0; i < words.length; i++) {
						String translation = dictionary.get(words[i]);
						if (translation == null) {
							System.out.print("? ");
						} else {
							System.out.print(translation + " ");
						}
					}
					System.out.println();
				}
			} catch (IOException e) {
				System.out.printf("you got an error m8 %d\n", e);
			}

		} else {
			System.out.println("first argument is supposed to be a file with dictionary, second a file with text to translate");
		}
	}
}