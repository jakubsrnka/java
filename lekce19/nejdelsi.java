import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class nejdelsi {
	private static String longest(FileReader reader) {
		String longest = "";
		Scanner sc = new Scanner(reader);
		while (sc.hasNextLine()) {
			String newLine = sc.nextLine();
			if (newLine.length() > longest.length()) {
				longest = newLine;
			}
		}
		return longest;
	}

	public static void main(String[] args) throws IOException {
		String longest = null;
		for (int i = 0; i < args.length; i++) {
			try {
				FileReader reader = new FileReader(args[i]);
				String longestInFile = longest(reader);
				reader.close();

				if ((longest == null) || longestInFile.length() > longest.length()) {
					longest = longestInFile;
				}
			} catch (IOException e) {
				System.out.println("solve your problems");
			}

		}

		if (longest != null) {
			System.out.println(longest);
		}
		
	}
}