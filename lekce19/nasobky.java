import java.io.IOException;
import java.io.PrintWriter;

public class nasobky {
    private static final int MAX_ON_LINE = 10;
	public static void main(String[] args) throws IOException{
        PrintWriter writer = new PrintWriter("nasobky.txt");
		int orderOnLine = 0;
		for (int i = 0; i < 1000; i += 3) {
			if (orderOnLine > 0) {
				writer.printf(", ");
                if (orderOnLine + 1 == MAX_ON_LINE) {
                    writer.println();
                    orderOnLine = 0;
                }
			}
            orderOnLine++;
            writer.printf("%d", i);
		}
        writer.println();
        writer.close();
	}
}