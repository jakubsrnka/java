import java.io.FileReader;
import java.io.IOException;


public class minmax {
	public static void main(String[] args) throws IOException{
		FileReader reader = new FileReader("cisla.txt");
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		java.util.Scanner sc = new java.util.Scanner(reader);
		while (sc.hasNextInt()) {
			int newNum = sc.nextInt();
			if (newNum > max) {
				max = newNum;
			}
			if (newNum < min) {
				min = newNum;
			}
		}
		reader.close();

		if (min > max) {
			System.out.println("nope");
		} else {
            System.out.printf("min = %d, max = %d\n", min, max);
		}
	}
}