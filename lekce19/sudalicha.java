import java.io.IOException;
import java.io.PrintWriter;

public class sudalicha {
	private static void printnums (String file, int min, int max, int step) throws IOException {
		PrintWriter writer = new PrintWriter(file);
		for (int i = min; i < max; i += step) {
			writer.printf("%d", i);
			writer.printf(" ", i);
		}
		writer.println();
		writer.close();
	}
	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.out.println("max missing");
			System.exit(1);
		}

		int max  = Integer.parseInt(args[0]);
		printnums("suda.txt", 0, max, 2);
		printnums("licha.txt", 1, max, 2);
	}
}