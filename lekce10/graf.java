public class graf {
	public static void main(String[] args) {
		awh.IntList numbers = awh.IntList.create();
		double biggest = 0;
		int number;
		double denominator = 1;


		java.util.Scanner sc = new java.util.Scanner(System.in);
		while (sc.hasNextInt()) {
			number = sc.nextInt();
			if (number >= 0) {
				numbers.add(number);
			}
			if (number > biggest) {
				biggest = number;
			}
		}

		if (biggest > 60) {
			denominator = 60/biggest;
		}

		for (int i = 0; i < numbers.size(); i++) {
			for (int j = 0; j < Math.ceil(numbers.get(i)*denominator); j++) {
				System.out.print("#");
			}
			System.out.println();
		}

	}
}