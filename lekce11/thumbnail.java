public class thumbnail {
    public static void main(String[] args) {
    	awh.Image img = awh.Image.loadFromFile(args[0]);
    	double height = (double) img.getWidth();
    	double width =  (double) img.getHeight();
    	double imgRatio = height/width;

        int outputHeight = 300;
        int outputWidth = 150;

        System.out.printf("%20.16f\n", imgRatio);


        String dimensions;
        if (args.length == 3) {
            dimensions = args[1];
            String[] dimensionsArray = dimensions.split("[x!]");
            outputWidth = Integer.parseInt(dimensionsArray[0]);
            outputHeight = Integer.parseInt(dimensionsArray[1]);
            if (dimensions.indexOf("!") != 0) {
                img.rescale(outputHeight, outputWidth); 
            }
        } else {


        double ratio = outputHeight/outputWidth;


    	if (ratio == imgRatio && args.length == 2){
    		img.rescale(outputHeight, outputWidth);
        } else {
    		awh.Color gray = awh.Color.fromRgb((int) (128), (int) 128, (int)128);
    		awh.Image thumbnail = awh.Image.createEmpty​(outputHeight, outputWidth, gray);

                double newHeight = outputHeight;
                double newWidth = outputWidth;
            if (imgRatio < ratio) {
                newWidth = outputHeight*(imgRatio/ratio);
            } else {
                newHeight = outputWidth*(ratio/imgRatio);
            }
    			img.rescale((int) Math.round(newWidth), (int) Math.round(newHeight));
                double indentHeight = (outputHeight - img.getWidth())/2;
    			double indentWidth = (outputWidth - img.getHeight())/2;
    			thumbnail.pasteFrom(img, (int) Math.round(indentHeight), (int) Math.round(indentWidth));
    		img = thumbnail;
    	}
    }   

        if (args.length == 3) {
            img.saveToFile(args[2]);
        } else {
            img.saveToFile(args[1]);
        }


    }
}