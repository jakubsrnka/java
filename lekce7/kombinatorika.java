public class kombinatorika {
    public static void main(String[] args) {
    	int n = Integer.parseInt(args[0]);
    	int k = Integer.parseInt(args[1]);
    	if (n == k) {
    		System.out.println("1");
    	} else if(k > n) {
    		System.out.println("0");
    	} else {
    		int kombinace = factorial(n)/(factorial(n-k)*factorial(k));
    		System.out.println(kombinace);
    	}
    }
    public static int factorial(int n) {
    	if (n <= 2) {
    	    return n;
    	}
   		return n * factorial(n - 1);
	}
}