public class sqrt {
    public static void main(String[] args) {
    	double root = Double.parseDouble(args[0]);
    	double error = Double.parseDouble(args[1]);
    	double approximated = (root/root+root)/2;
    	double sqrt = Math.sqrt(root);
    	while (sqrt + error < approximated) {
    		approximated = (root/approximated+approximated)/2;
    	}
    	System.out.println(approximated);
    }
}