public class krizek {
    public static void main(String[] args) {
    	int size = Integer.parseInt(args[0]);
    	narrow(size);
    	wide(size);
    	narrow(size);
    }
    public static void narrow(int size) {
		for (int i = 0; i < size; i++) {
    		write(size, ' ');
    		write(size, 'X');
    		write(size, ' ');
    		System.out.print("\n");
    	}
    }
    public static void wide(int size) {
		for (int i = 0; i < size; i++) {
    		write(size*3, 'X');
    		System.out.print("\n");
    	}
    }
    public static void write(int size, char character) {
    	for (int i = 0; i < size; i++) {
    		System.out.print(character);
    	}
    }
}