public class den {
    public static void main(String[] args) {
		int year = Integer.parseInt(args[0]);
		int month = Integer.parseInt(args[1]);
		int day = Integer.parseInt(args[2]);
		int days = 0;
		int[] daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    	if ((year%4 == 0) && (year%100 != 0)) {
			daysInMonth[1] = 29;
		}

		for (int i = 0; i < month-1; i++) {
			days += daysInMonth[i];
		}

		days += day;
		System.out.println(days);
    }
}