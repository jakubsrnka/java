public class MathUtils {
 
    public static int min(int... numbers) {
        int result = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < result) {
                result = numbers[i];
            }
        }
        return result;
    }
 
    public static int gcd(int a, int b) {
    	int d = 0;
    	while (a == b && a%2 == 0) {
    		a = a/2;
    		b = b/2;
    		d++;
    	}
    	while (a != b) {
    		if (a % 2 == 0) {
    			a = a/2;
    		} else if (b % 2 == 0) {
    			b = b/2;
    		} else if (a>b) {
    			a = (a-b)/2;
    		} else {
    			b = (b-a)/2;
    		}
    	}
    	int gcd = a * Math.pow(2, d);
        return a;
    }
}