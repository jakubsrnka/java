import java.util.Scanner;
import java.io.IOException;

public class email {
	public static void main(String[] args) throws IOException {
		try {
			Scanner sc = new Scanner(java.nio.file.Paths.get("email.txt"));


		Boolean nextIsSender = false;
		Boolean nextIsSubject = false;
		String sender = "";
		String subject = "";
		

		while (sc.hasNext()) {
			String next = sc.next();
			if (nextIsSender && next.contains("Subject")) {
				nextIsSender = false;
			} else if (nextIsSender) {
				sender += next + " ";
			}
			if (nextIsSubject && next.contains("To")) {
				nextIsSubject = false;
			} else if (nextIsSubject) {
				subject += next + " ";
			}
			if (next.contains("From")) {
				nextIsSender = true;
			}
			if (next.contains("Subject")) {
				nextIsSubject = true;
			}
		}
		System.out.printf("sender: %s\nsubject: %s\n", sender, subject);

		} catch (IOException e) {
			System.out.printf("you got an error m8 %d\n", e);
		}

	}
}