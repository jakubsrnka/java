import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
public class Kalendar {
	public static void main(String[] args) { //vezme od uzivatele 0, 1, nebo 2 args, spocita spravny mesic a rok a necha pomoci printMonth() vypsat tri po sobe jdouci mesice
		int argsLen = args.length;
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter yearFormat = DateTimeFormatter.ofPattern("yyyy");
		DateTimeFormatter monthFormat = DateTimeFormatter.ofPattern("MM");
		int year = Integer.parseInt(time.format(yearFormat));
		int month = Integer.parseInt(time.format(monthFormat));
		switch (argsLen) {
			case 1:
				month = Integer.parseInt(args[0]);
				break;
			case 2:
				month = Integer.parseInt(args[0]);
				year = Integer.parseInt(args[1]);
				break;
		}
		printMonth(month == 1 ? 12 : month - 1, month == 1 ? year - 1 : year);
		printMonth(month, year);
		printMonth(month == 12 ? 1 : month + 1, month == 12 ? year + 1 : year);
	}
	public static void printMonth(int month, int year) { //dostane mesic a rok, pote vypise dany mesic jako cast kalendare se spravnym jmenem mesice a rokem
		String[] monthsArray = {"leden", "unor", "brezen", "duben", "kveten", "cerven", "cervenec", "srpen", "zari", "rijen", "listopad", "prosinec"};
		int[] daysInMonthArray = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    	if ((year%4 == 0) && (year%100 != 0)) daysInMonthArray[1] = 29;
		String monthStr = String.format("%02d", month);
		String str = year + monthStr + "01";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDate dateTime = LocalDate.parse(str, formatter);
		DayOfWeek dayOfWeek = DayOfWeek.from(dateTime);
		int dayInt = dayOfWeek.getValue();
		System.out.println("+----------------------+");
		System.out.print("| "+monthsArray[month-1]+" "+year); //vypisuje jmeno mesice a rok
		for (int i=0; i<22-(monthsArray[month-1].length()+6); i++) {
			System.out.print(" ");
		}
		System.out.println("|");
		System.out.println("+----------------------+");
		int currentDay = 1;
		int daysInMonth = daysInMonthArray[month-1];
		double weeks = Math.ceil((daysInMonth+dayInt-1)/7.0);
		for (int i=0; i<weeks; i++) { //vypisuje kalendar
			System.out.print("|");
			if (i == 0) {
				for (int k=0; k<dayInt-1; k++) {
					System.out.print("   ");
				}
				for (int j=0; j<=7-dayInt; j++) {
					String day = String.format("%02d", currentDay);
					System.out.print(" " + day);
					currentDay++;
				}
			} else if (i<weeks-1 || ((daysInMonth+dayInt-1)%7) == 0) {
				for (int j=0; j<7; j++) {
					String day = String.format("%02d", currentDay);
					System.out.print(" " + day);
					currentDay++;
				}
			} else if ((7-((daysInMonth+dayInt)%7)) != 0) {
				for (int j=currentDay; j<=daysInMonth; j++) {
					String day = String.format("%02d", currentDay);
					System.out.print(" " + day);
					currentDay++;
				}
				for (int k=0; k<=(6-((daysInMonth+dayInt-1)%7)); k++) {
					System.out.print("   ");
				}
			}
			System.out.print(" |");
			System.out.println();
		}
		System.out.println("+----------------------+");
	}
}