public class Pascal {
	public static void main(String[] args) { //vezme si od uzivatele pocet radku, vypise pyramidu
		int rows = Integer.parseInt(args[0]);
		int biggestNumber = factorial(rows)/(factorial(rows/2)*factorial(rows/2));
		int current = 1;
		for (int i=0; i<rows; i++) {
			System.out.printf("%d ", current);
			for (int j=1; j<=(i/2); j++) {
				current = factorial(i)/(factorial(j)*factorial(i-j));
				System.out.printf("%d ", current);
			}
			if (i%2 == 0) {
				for (int j=(i/2)-1; j>0; j--) {
					current = factorial(i)/(factorial(j)*factorial(i-j));
					System.out.printf("%d ", current);
				}
			} else {
				for (int j=(i/2); j>0; j--) {
					current = factorial(i)/(factorial(j)*factorial(i-j));
					System.out.printf("%d ", current);
				}
			}
			current=1;
			if (i > 0) System.out.printf("%d ", current);
			System.out.print("\n");
		}
		System.out.printf("%d\n", biggestNumber);
	} //vypocita faktorial cisla n v argumentu
	public static int factorial(int n) {
    	if (n <= 2) return n;
   		return n * factorial(n - 1);
	}
}