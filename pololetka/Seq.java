public class Seq {
    public static void main(String[] args) {
        int argsLen = args.length;
        int first = 1;
        int step = 1;
        int last = 2;
        String gap = " ";
        if (argsLen > 0 && args[0].equals("-s")) { //necha vypsat sekvenci s uzivatelem danym oddelovatelem
            gap = (args.length == 1 ? " " : args[1]);
            switch (argsLen) {
                case 3:
                    last = Integer.parseInt(args[2]);
                    break;
                case 4:
                    first = Integer.parseInt(args[2]);
                    last = Integer.parseInt(args[3]);
                    break;
                case 5:
                    first = Integer.parseInt(args[2]);
                    step = Integer.parseInt(args[3]);
                    last = Integer.parseInt(args[4]);
                    break;
            }
        } else { //necha vypsat sekvenci s mezerou jako oddelovacem
            switch (argsLen) {
                case 1:
                    last = Integer.parseInt(args[0]);
                    break;
                case 2:
                    first = Integer.parseInt(args[0]);
                    last = Integer.parseInt(args[1]);
                    break;
                case 3:
                    first = Integer.parseInt(args[0]);
                    step = Integer.parseInt(args[1]);
                    last = Integer.parseInt(args[2]);
                    break;
            }
        }
        System.out.print(first);
        for (int i=first+step; i<=last; i+=step) { //vypise sekvenci
            System.out.print(gap + i);
        }
        System.out.println();
    }
}