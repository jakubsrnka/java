public class Odmocnina {
    public static void main(String[] args) {
    	double root = Double.parseDouble(args[0]);
    	double error = (args.length == 1 ? 0.001 : Double.parseDouble(args[1]));
    	double approximated = ((root/(root*root))+2*root)/3;
    	double cbrt = Math.cbrt(root);
    	while (cbrt + error < approximated) {
    		approximated = ((root/(approximated*approximated))+2*approximated)/3;
    	}
    	System.out.println(approximated);
    }
}