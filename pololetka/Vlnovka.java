public class Vlnovka {
    public static void main(String[] args) {
    	int size = Integer.parseInt(args[0]);
    	int offsetStart = 0;
    	int offsetEnd = 0;
    	int offsetMiddle = 0;
    	int sequence = (size*2)-2;
    	double repeat = Math.floor(80/sequence);
    	int residue = 80%sequence;
    	for (int i=0; i<size; i++) { //pocet radku
    		for (int o=0; o<repeat; o++) { //vypise cele kusy "v"
    			if (i == 0) {
    				System.out.print("X");
    				for (int m=0; m<(sequence-1); m++) {
    					System.out.print(" ");
    				}
    			} else if (i == size-1) {
    				for (int m=0; m<(size-1); m++) {
    					System.out.print(" ");
    				}
    				System.out.print("X");
    				for (int n=0; n<(size-2); n++) {
    					System.out.print(" ");
    				}
    			} else {
    				offsetStart = i;
    				offsetEnd = i-1;
    				offsetMiddle = sequence - (offsetStart + offsetEnd + 2);
    				for (int j=0; j<(offsetStart); j++) {
    					System.out.print(" ");
    				}
    				System.out.print("X");
    				for (int l=0; l<offsetMiddle; l++) {
    					System.out.print(" ");
    				}
    				System.out.print("X");
    				for (int k=0; k<(offsetEnd); k++) {
    					System.out.print(" ");
    				}
    			}
    		}
    		if (residue > 0) { //vypise zbyly kus posledniho "v", pokud je potreba
    			String rest = "";
    			if (i == 0) {
    				rest += "X";
    			} else if (i == size-1) {
    				for (int m=0; m<(size-1); m++) {
    					rest += " ";
    				}
    				rest += "X";
    			} else {
    				offsetStart = i;
    				offsetEnd = i-1;
    				offsetMiddle = sequence - (offsetStart + offsetEnd + 2);
    				for (int j=0; j<(offsetStart); j++) {
    					rest += " ";
    				}
    				rest += "X";
    				for (int l=0; l<offsetMiddle; l++) {
    					rest += " ";
    				}
    				rest += "X";
    			}
    			if (rest.length() < residue) {
    				System.out.print(rest);
    			} else {
    				System.out.print(rest.substring(0, residue));
    			}

    		}
           	System.out.print("\n");
    	}
    }
}